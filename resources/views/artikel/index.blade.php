@extends('layouts.app')
@push('customcss')
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}"></script>
@endpush
  @section('title','Dashboard')
  @section('page-title','Artikel')
  @section('content')
  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Artikel</h3>
      <div class="pull-right">
          <a href="{{ route('artikel.create') }}" class="btn btn-success">Tambah Artikel</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="myTable" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Gambar</th>
          <th>Nama Kategori</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
          @foreach ($artikel as $item)
          <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->judul}}</td>
                    
                    @if($item->gambar==null)
                    <td>Gambar tidak ada</td>
                    @else
                    <td><img src="{{ asset('uploads/'.$item->gambar) }}" width="50px" height="50px" ></td>
                    @endif

                    <td>{{$item->Kategori->nama_kategori}}</td>
                    <td>
                        <a href="{{ route('artikel.edit',$item->id) }}" class="btn btn-info">Edit</a>
                        
                        <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger">
                                <span class="fa fa-trash"></span>
                                <form action="{{ route('artikel.destroy', $item->id) }}" method="POST">
                                     @csrf

                                     @method('DELETE')
                                </form>

                        </a>
                    </td>
                </tr>
          @endforeach
             
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  @push('datatables')

  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables/datatables.bootstrap.min.js') }}"></script>
  @endpush
  @push('customdatatables')
  <script>
    $(document).ready( function () {
    $('#myTable').DataTable();
    
    } );
  </script>
<!-- alert sweetAlert -->
 <!-- <script>
   $('.delete').click(function(){
    swal({
      title: "Yakin ?",
      text: "Mau dihapus data artikel ini!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Poof! Your imaginary file has been deleted!", {
          icon: "success",
        });
      } else {
        swal("Your imaginary file is safe!");
      }
    });
   });
 </script> -->
  @endpush
  @endsection